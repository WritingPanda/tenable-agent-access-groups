This script is a test script for Tenable.io. It is not supported by Tenable.

The idea is to get agent groups and use that list to generate access groups. Then, a user can add those hosts to the appropriate access groups.

This is only an MVP. Needs additional work to be fully functional. 

To get the dependencies, run `pip install -r requirements.txt`. This script has only been tested on Python 3.7.5.

There is the jupyter notebook dependencies here, too, but that is because if you want to run a notebook to play with the API, the option is there. It's not necessary to run the main script, however. If you are running into issues, it's a good way to play with the API and troubleshoot.

Please make sure to update the `settings-example.ini` file by renaming it to `settings.ini` and adding your `ACCESS_KEY` and `SECRET_KEY` from Tenable.io. If you save this to a remote repository, please make sure you keep `settings.ini` untracked and ignored by Git.
