import aiohttp
import asyncio
import json
import logging
from decouple import config
from requests import Session


"""
Requirements
1. Create an agent group (done in UI).
2. Create an access group of the same name as the agent group (done via API).
3. Add agents within specific groups to the access group (done via API).

Assumptions with this script:
1. The admin has installed agents on hosts and linked them to Tenable.io
2. The admin has created agent groups and organized the agents into those groups
3. The admin has not created access groups and wants to create access groups based on the agent groups
"""

logging.basicConfig(level=logging.INFO, 
                    format='[%(levelname)s:%(asctime)s:%(funcName)25s():%(lineno)s] %(message)s',
                    datefmt='%d-%b-%y %H:%M:%S')
logger = logging.getLogger(__name__)


# Generator function for async for loops
async def aiter(items):
    for item in items:
        yield item


# Create the session
def create_session(test:bool=False) -> Session:
    session = Session()
    session.headers.update(HEADERS)
    if test == True:
        url = BASE + 'session'
        logging.info(f"Request to {url}.")
        session_test_response = session.get(url)
        if session_test_response.status_code != 200:
            logging.error("Your access key or secret key are incorrect. Please re-run the script and try again.")
        else:
            return session
    else:
        return session


def get_access_groups(session:Session) -> list:
    url = BASE + 'access-groups'
    logging.info(f"Request to {url}.")
    access_groups_raw = session.get(url).json()['access_groups']
    access_groups = [{"id": ag["id"], "name": ag["name"]} for ag in access_groups_raw]
    if len(access_groups) == 0:
        logger.warning("There are no access groups on this account.")
        return list()
    else:
        return access_groups


def get_agent_groups(session:Session) -> list:
    url = BASE + 'scanners/1/agent-groups'
    logging.info(f"Request to {url}.")
    groups = session.get(url).json()['groups']
    group_ids_names = [{'id': group['id'], 'name': group['name']} for group in groups]
    return group_ids_names


def get_access_group_id_by_name(session:Session, access_group_name:str) -> tuple:
    access_groups = get_access_groups(session)
    access_groups_ids_names = [{"id": ag['id'], "name": ag['name']} for ag in access_groups]
    for ag_id_name in access_groups_ids_names:
        if access_group_name == ag_id_name['name']:
            return ag_id_name


def get_names_from_group(group:list) -> list:
    names_in_group = [g['name'] for g in group]
    return names_in_group


def diff_agent_access_group_names(agent_groups:list, access_groups:list) -> list:
    access_group_names = get_names_from_group(access_groups)
    agent_group_names = get_names_from_group(agent_groups)
    diff = [i for i in access_group_names + agent_group_names if i not in access_group_names or i not in agent_group_names and i != 'All Assets']
    return diff


async def create_access_group_based_on_agent_groups(group_name:str) -> None:
    payload = json.dumps({
        "access_group_type": "MANAGE_ASSETS",
        "name": f"{group_name}"
    })
    url = BASE + 'access-groups'
    async with LIMIT:
        async with aiohttp.ClientSession(headers=HEADERS) as session:
            logging.info(f"POST Request to {url} with payload: {payload}.")
            async with session.post(url, data=payload) as response:
                if response.status != 200:
                    logging.error(f"The request to {BASE}access-groups/ did not go through properly. STATUS CODE: {response.status}")


def generate_urls(agent_groups:list) -> list:
    base_url = BASE + "scanners/1/agent-groups/{g_id}"
    urls = [base_url.format(g_id=group['id']) for group in agent_groups]
    return urls


def combine_access_group_and_agent_group_data(access_groups:list, agent_groups:list) -> list:
    combined_list = list()
    for agent_group in agent_groups:
        for access_group in access_groups:
            if access_group['name'] == agent_group['group_name']:
                combined_list.append(
                    {
                        'group_name': agent_group['group_name'],
                        'agents': agent_group['agents'],
                        'access_group_id': access_group['id'],
                    }
                )
                break
    return combined_list


async def fetch_agents_from_group(url:str) -> None:
    async with LIMIT:
        async with aiohttp.ClientSession(headers=HEADERS) as session:
            logging.info(f"Request to {url}.")
            async with session.get(url) as response:
                response = await response.json()
                if len(response['agents']) > 0:
                    GROUPED_AGENTS.append({"group_name": response['name'], "agents": [agent['name'] for agent in response['agents']]})


async def add_agents_from_group_to_access_group(group_name:str,
                                                agent_hostnames:list,
                                                access_group_id:str) -> None:
    """
    To add an agent to the access group, you need three things:
    1. ID access group
    2. Name of the access group
    3. Hostname of the agent
    Hostname of the agent is for the rule. Agent name must be the hostname of the machine.
    Another function can be added to this script that will allow the use
    of IPv4 addresses to be use as a part of the rule set. This is not recommended as
    IPv4 addresses are temporary unless statically assigned.
    Before this function can be run, there must be access groups that exist
    with the same name as the agent groups.
    """
    payload = json.dumps({
        "name": group_name,
        "all_assets": False,
        "all_users": False,
        "rules": [
            {
                "type": "hostname",
                "operator": "match",
                "terms": agent_hostnames
            }
        ]
    })
    url = BASE + f"access-groups/{access_group_id}"
    
    async with LIMIT:
        async with aiohttp.ClientSession(headers=HEADERS) as session:
            logging.info(f"Request to {url} with payload: {payload}")
            async with session.put(url, data=payload) as response:
                if response.status != 200:
                    logging.error(f"The request to {BASE}access-groups/{access_group_id} did not go through properly. STATUS CODE: {response.status}")        


if __name__ == '__main__':
    BASE = "https://cloud.tenable.com/"
    HEADERS = {
        'accept': 'application/json',
        'content-type': 'application/json',
        'x-apikeys': f"accessKey={config('ACCESS_KEY')};secretKey={config('SECRET_KEY')}"
    }
    # This is necessary to save the data retrieved from agent details and then broken down by agent group
    GROUPED_AGENTS = list()
    LIMIT = asyncio.Semaphore(200)
    
    session = create_session()
    agent_groups = get_agent_groups(session)
    agent_urls = generate_urls(agent_groups)
    access_groups= get_access_groups(session)
    # Identify any group names that are different between the agent groups and access groups
    diff_list = diff_agent_access_group_names(agent_groups, access_groups)

    loop = asyncio.get_event_loop()

    # Create access groups based on agent groups
    loop.run_until_complete(asyncio.gather(*[create_access_group_based_on_agent_groups(group_name=group_name) for group_name in diff_list]))
    # Get agent group details and put it in a list
    loop.run_until_complete(asyncio.gather(*[fetch_agents_from_group(url) for url in agent_urls]))
    # Combine agent group data and access group data to update access groups with agents from within the group
    new_access_groups = get_access_groups(session)
    combined_data = combine_access_group_and_agent_group_data(new_access_groups, GROUPED_AGENTS)
    # Update access groups with agents
    loop.run_until_complete(asyncio.gather(*[add_agents_from_group_to_access_group(
        group_name=data['group_name'],
        agent_hostnames=data['agents'],
        access_group_id=data['access_group_id']
    ) for data in combined_data]))
    loop.close()
