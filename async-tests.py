import aiohttp
import asyncio
import logging
from decouple import config
from requests import Session


logging.basicConfig(level=logging.INFO, 
                    format='[%(levelname)s:%(asctime)s:%(funcName)25s():%(lineno)s] %(message)s',
                    datefmt='%d-%b-%y %H:%M:%S')
logger = logging.getLogger(__name__)
BASE = "https://cloud.tenable.com/"
HEADERS = {
    "accept": "application/json",
    "content-type": "application/json",
    "x-apikeys": f"accessKey={config('ACCESS_KEY')};secretKey={config('SECRET_KEY')}"
}
GROUPED_AGENTS = list()


async def aiter(items):
    for item in items:
        yield item


# Create the session
def create_session(test:bool=False) -> Session:
    session = Session()
    session.headers.update(HEADERS)
    if test == True:
        url = BASE + 'session'
        logging.info(f"Request to {url}.")
        session_test_response = session.get(url)
        if session_test_response.status_code != 200:
            logging.error("Your access key or secret key are incorrect. Please re-run the script and try again.")
        else:
            return session
    else:
        return session


def get_agent_groups(session:Session) -> list:
    url = BASE + 'scanners/1/agent-groups'
    logging.info(f"Request to {url}.")
    groups = session.get(url).json()['groups']
    group_ids_names = [{'id': group['id'], 'name': group['name']} for group in groups]
    return group_ids_names


def generate_urls(agent_groups:list) -> list:
    base_url = BASE + "scanners/1/agent-groups/{g_id}"
    urls = [base_url.format(g_id=group['id']) for group in agent_groups]
    return urls


async def fetch_agents_from_group(url:str, limit:asyncio.Semaphore) -> None:
    async with limit:
        async with aiohttp.ClientSession(headers=HEADERS) as session:
            logging.info(f"Request to {url}")
            async with session.get(url) as response:
                response = await response.json()
                if len(response['agents']) > 0:
                    GROUPED_AGENTS.append({"group_name": response['name'], "agents": [agent['name'] for agent in response['agents']]})


if __name__ == '__main__':
    limit = asyncio.Semaphore(200)
    session = create_session()
    agent_groups = get_agent_groups(session)
    urls = generate_urls(agent_groups)

    loop = asyncio.get_event_loop()
    loop.run_until_complete(asyncio.gather(*[fetch_agents_from_group(url, limit) for url in urls]))
    loop.close()
